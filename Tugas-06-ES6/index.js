// Soal 1
// buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini

// jawaban 1
const persegiPanjang = (p, l) => {
  let luas, keliling;

  luas = p * l;
  keliling = 2 * (p + l);

  console.log(`Luas persegi panjang adalah : ${luas}`);
  console.log(`Keliling persegi panjang adalah : ${keliling}`);
};
const panjang = 2;
const lebar = 5;
persegiPanjang(panjang, lebar);

// Soal 2
// Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana

// const newFunction = function literal(firstName, lastName) {
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function () {
//       console.log(firstName + ' ' + lastName);
//     },
//   };
// };

// jawaban 2
const newFunction = (firstName, lastName) => {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`);
    },
  };
};

//Driver Code
newFunction('William', 'Imoh').fullName();

// Soal 3
// Diberikan sebuah objek sebagai berikut:
const newObject = {
  firstName: 'Garin',
  lastName: 'Yudha Ramadika',
  address: 'Jalan Azelia',
  hobby: 'playing games',
};

// jawaban 3
const { firstName, lastName, address, hobby } = newObject;
// Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)
// Driver code
console.log(firstName, lastName, address, hobby);

// soal 4
// Kombinasikan dua array berikut menggunakan array spreading ES6
const west = ['Will', 'Chris', 'Sam', 'Holly'];
const east = ['Gill', 'Brian', 'Noel', 'Maggie'];

// jawaban 4
let combined = [...west, ...east];
//Driver Code
console.log(combined);

// soal 5
// sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

const planet = 'earth';
const view = 'glass';
var before =
  'Lorem ' +
  view +
  'dolor sit amet, ' +
  'consectetur adipiscing elit,' +
  planet;

// jawaban 5
let after = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`;

console.log(before);
console.log(after);
