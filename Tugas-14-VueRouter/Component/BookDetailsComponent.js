export const BooksDetailsComponent = {
  data() {
    return {
      books: [
        { id: 1, name: 'Kalkulus Dasar', stock: 55, price: 50000 },
        { id: 2, name: 'Fisika Dasar', stock: 55, price: 55000 },
        { id: 3, name: 'Aljabar Linier Elementer', stock: 55, price: 20000 },
        { id: 4, name: 'Aljabar Abstrak', stock: 55, price: 50000 },
        { id: 5, name: 'Alogaritma Pemrograman', stock: 55, price: 100000 },
      ],
    };
  },
  computed: {
    book() {
      return this.books.filter((book) => {
        return book.id === parseInt(this.$route.params.id);
      });
    },
  },
  template: `
  <div>
    <div class="card">
      <div class="card-header">
        Detail Book
      </div>
      <div class="card-body">
        <table class="table">
          <thead>
          <h3></h3>
            <tr>
              <th scope="col">Book Title</th>
              <th scope="col">Stock</th>
              <th scope="col">Price</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="(index, value) in book">
              <td>{{ index.name }}</td>
              <td>{{ index.stock }}</td>
              <td>{{ index.price }}</td>
              <td>  
                <router-link 
                class="btn btn-success" :to="'/buy/'+index.id"> 
                Buy
                </router-link>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
      `,
};
