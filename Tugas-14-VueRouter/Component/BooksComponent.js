export const BooksComponent = {
  data() {
    return {
      books: [
        { id: 1, name: 'Kalkulus Dasar', stok: 55 },
        { id: 2, name: 'Fisika Dasar', stok: 55 },
        { id: 3, name: 'Aljabar Linier Elementer', stok: 55 },
        { id: 4, name: 'Aljabar Abstrak', stok: 55 },
        { id: 5, name: 'Alogaritma Pemrograman', stok: 55 },
      ],
    };
  },
  template: `
  <div>
    <div class="card">
      <div class="card-header">
        List Books
      </div>
      <div class="card-body">
        <table class="table">
          <thead>
          <h3></h3>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Book Title</th>
              <th scope="col">Stock</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="book in books">
              <td>{{ book.id }}</td>
              <td>{{ book.name }}</td>
              <td>{{ book.stok }}</td>
              <td>  
                <router-link 
                class="btn btn-primary" :to="'/books/'+book.id+'/detail'"> 
                Detail
                </router-link>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
      `,
};
