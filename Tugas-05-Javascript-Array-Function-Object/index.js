// soal 1
var daftarHewan = ['2. Komodo', '5. Buaya', '3. Cicak', '4. Ular', '1. Tokek'];
// urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):

// 1. Tokek
// 2. Komodo
// 3. Cicak
// 4. Ular
// 5. Buaya

// jawaban 1
var listHewan = daftarHewan.sort();
listHewan.forEach(function (list) {
  console.log(list);
});

// soal 2

// jawaban 2
function introduce(data) {
  return (
    'Nama saya ' +
    data.name +
    ', umur saya ' +
    data.age +
    ' tahun, alamat saya di ' +
    data.address +
    ', dan saya punya hobby yaitu ' +
    data.hobby
  );
}

var data = {
  name: 'John',
  age: 30,
  address: 'Jalan Pelesiran',
  hobby: 'Gaming',
};

var perkenalan = introduce(data);
console.log(perkenalan);
// expected output
// Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

// soal 3

// Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.

// jawaban 3
function hitung_huruf_vokal(nama) {
  if (typeof nama == 'string') {
    var hurufVocal = '';

    var namaLowerCase = nama.toLowerCase();
    var splitNama = namaLowerCase.split('');

    splitNama.forEach(function (item) {
      if (
        item == 'a' ||
        item == 'i' ||
        item == 'u' ||
        item == 'e' ||
        item == 'o'
      ) {
        hurufVocal += item;
      }
    });

    return hurufVocal.length;
  } else {
    return 'Hanya boleh karakter';
  }
}

var hitung_1 = hitung_huruf_vokal('Muhammad'); // 3
var hitung_2 = hitung_huruf_vokal('Iqbal'); // 2
var hitung_3 = hitung_huruf_vokal(21);
console.log(hitung_1, hitung_2, hitung_3);

// soal 4

// Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan output sebagai berikut.

// jawaban 4
function hitung(angka) {
  if (typeof angka == 'number') {
    var nilai = 2;

    var hasil = nilai * angka - nilai;

    return hasil;
  } else {
    return 'Hanya boleh angka';
  }
}

console.log(hitung(0)); // -2
console.log(hitung(1)); // 0
console.log(hitung(2)); // 2
console.log(hitung(3)); // 4
console.log(hitung(5)); // 8
console.log(hitung('test'));
