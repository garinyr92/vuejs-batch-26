// Soal 2
var readBooksPromise = require('./promise.js');

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
];

var books2 = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
  { name: 'komik', timeSpent: 1000 },
];

// Lanjutkan code untuk menjalankan function readBooksPromise

// Jawaban 2

let readTime = 10000; //waktu baca
let i = 0;

const readPromise = (time) => {
  readBooksPromise(time, books[i])
    .then((timeLeft) => {
      i++;
      if (timeLeft >= 0 && i < books.length) {
        readPromise(timeLeft);
      }
    })
    .catch((error) => console.log(error));
};

// readPromise(readTime);

// example async await

// example async await
const execute = async () => {
  const timeleft0 = await readBooksPromise(readTime, books2[0]);
  const timeleft1 = await readBooksPromise(timeleft0, books2[1]);
  const timeleft2 = await readBooksPromise(timeleft1, books2[2]);
  const timeleft3 = await readBooksPromise(timeleft2, books2[3]);

  return console.log('selesai, sisa waktu ' + timeleft3);
};

execute();
