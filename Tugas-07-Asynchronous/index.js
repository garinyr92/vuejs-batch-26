// Soal 1
var readBooks = require('./callback.js');

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
  { name: 'komik', timeSpent: 1000 },
];

// Tulis code untuk memanggil function readBooks di sini

// Jawaban 1

const readTime = 10000; //waktu baca
let i = 0;

const readCallback = (time) => {
  readBooks(time, books[i], (timeLeft) => {
    i++;
    if (timeLeft >= 0 && i < books.length) {
      readCallback(timeLeft);
    }
  });
};

readCallback(readTime);
