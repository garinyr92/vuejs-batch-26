export const MembersComponent = {
  template: `
  <table class="table">
  <thead>
    <tr>
      <th>Photo</th>
      <th>Name</th>
      <th>Address</th>
      <th>Phone</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <img
          width="100"
          height="100"
          :src="member.photo_profile ? photoDomain +  member.photo_profile  : 'https://dummyimage.com/100'"
          alt=""
        />
      </td>
      <td>{{member.name}}</td>
      <td>{{member.address}}</td>
      <td>{{member.no_hp}}</td>
      <td>
        <button
          class="btn btn-info"
          @click="$emit('edit',member)"
        >
          Edit
        </button>
        <button
          class="btn btn-danger"
          @click="$emit('delete',member.id)"
        >
          Hapus
        </button>
        <button
          class="btn btn-secondary"
          @click="$emit('upload',member)"
        >
          Upload
        </button>
      </td>
    </tr>
  </tbody>
</table>
    `,
  props: ['member', 'btnStatus', 'photoDomain'],
};
