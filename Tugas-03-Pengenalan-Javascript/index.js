// soal 1
var pertama = 'saya sangat senang hari ini';
var kedua = 'belajar javascript itu keren';
// expected output
// saya senang belajar JAVASCRIPT

// jawaban soal 1

// opsi 1
// var text = pertama + ' ' + kedua;
// var textSplit = text.split(' ');
// var upper = textSplit[6].toUpperCase();
// var jawabanPertama =
//   textSplit[0] + ' ' + textSplit[2] + ' ' + textSplit[5] + ' ' + upper;
// console.log(jawabanPertama);

// opsi 2
var textPertama = pertama.substr(0, 4);
var textKedua = pertama.substr(12, 6);
var textKetiga = kedua.substr(0, 7);
var textKeempat = kedua.substr(8, 10);

var jawabanPertama =
  textPertama +
  ' ' +
  textKedua +
  ' ' +
  textKetiga +
  ' ' +
  textKeempat.toUpperCase();
console.log(jawabanPertama);

// soal 2
// edit text kata menjadi string karena sama dengan nama var soal 3
var stringPertama = '10';
var stringKedua = '2';
var stringKetiga = '4';
var stringKeempat = '6';
// expected output
//  (int) 24

// jawaban soal 2
// convert to int
var intPertama = parseInt(stringPertama);
var intKedua = parseInt(stringKedua);
var intKetiga = parseInt(stringKetiga);
var intKeempat = parseInt(stringKeempat);

var jawabanKedua = intPertama + intKedua * intKetiga + intKeempat;
console.log(jawabanKedua);

// soal 3
var kalimat = 'wah javascript itu keren sekali';
// expected output
// Kata Pertama: wah
// Kata Kedua: javascript
// Kata Ketiga: itu
// Kata Keempat: keren
// Kata Kelima: sekali

// jawaban soal 3
var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);
