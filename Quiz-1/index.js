// Soal 1
// Judul : Function Penghasil Tanggal Hari Esok

// Buatlah sebuah function dengan nama next_date() yang menerima 3 parameter tanggal, bulan, tahun dan mengembalikan nilai tanggal hari esok dalam bentuk string, dengan contoh input dan otput sebagai berikut.

// Jawaban 1

function next_date(tanggal, bulan, tahun) {
  // make date new date
  var date = new Date(tahun + '-' + bulan + '-' + tanggal);
  // set date + 1 day
  var tomorrowDate = date.setDate(date.getDate() + 1);
  // convert it to date
  var convertNewDate = new Date(tomorrowDate);

  var day = convertNewDate.getDate();
  var month = convertNewDate.getMonth() + 1;
  var year = convertNewDate.getFullYear();

  switch (month) {
    case 1: {
      console.log(day + ' ' + 'Januari' + ' ' + year);
      break;
    }
    case 2: {
      console.log(day + ' ' + 'Februari' + ' ' + year);
      break;
    }
    case 3: {
      console.log(day + ' ' + 'Maret' + ' ' + year);
      break;
    }
    case 4: {
      console.log(day + ' ' + 'April' + ' ' + year);
      break;
    }
    case 5: {
      console.log(day + ' ' + 'Mei' + ' ' + year);
      break;
    }
    case 6: {
      console.log(day + ' ' + 'Juni' + ' ' + year);
      break;
    }
    case 7: {
      console.log(day + ' ' + 'Juli' + ' ' + year);
      break;
    }
    case 8: {
      console.log(day + ' ' + 'Agustus' + ' ' + year);
      break;
    }
    case 9: {
      console.log(day + ' ' + 'September' + ' ' + year);
      break;
    }
    case 10: {
      console.log(day + ' ' + 'Oktober' + ' ' + year);
      break;
    }
    case 11: {
      console.log(day + ' ' + 'November' + ' ' + year);
      break;
    }
    case 12: {
      console.log(day + ' ' + 'Desember' + ' ' + year);
      break;
    }
    default: {
      console.log('Error');
    }
  }
}

var tanggal = 29;
var bulan = 02;
var tahun = 2020;

var tanggal2 = 28;
var bulan2 = 2;
var tahun2 = 2021;

var tanggal3 = 31;
var bulan3 = 12;
var tahun3 = 2020;

next_date(tanggal, bulan, tahun); // output : 1 Maret 2020
next_date(tanggal2, bulan2, tahun2); // output : 1 Maret 2021
next_date(tanggal3, bulan3, tahun3); // output :  1 Januari 2021

// Soal 2
// Judul : Function Penghitung Jumlah Kata

// Buatlah sebuah function dengan nama jumlah_kata() yang menerima sebuah kalimat (string), dan mengembalikan nilai jumlah kata dalam kalimat tersebut.
/*
Contoh;

var kalimat_1 = ' Halo nama saya Muhammad Iqbal Mubarok ';
var kalimat_2 = 'Saya Iqbal';

jumlah_kata(kalimat_1); // 6
jumlah_kata(kalimat_2); // 2
*/

// Jawaban 2

function jumlah_kata(kalimat) {
  if (typeof kalimat == 'string') {
    var textTrim = kalimat.trim();
    var text = textTrim.split(' ');
    console.log(text.length);
  } else {
    console.log('Harus menggunakan karakter');
  }
}

var kalimat_1 = ' Halo nama saya Muhammad Iqbal Mubarok ';
var kalimat_2 = 'Saya Iqbal';
var kalimat_3 = 1;
jumlah_kata(kalimat_1); // 6
jumlah_kata(kalimat_2); // 2
jumlah_kata(kalimat_3); // 2
