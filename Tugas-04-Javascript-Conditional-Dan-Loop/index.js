// soal 1
var nilai = 100;

// Jawaban soal 1
if (nilai >= 85 && nilai <= 100) {
  console.log('A');
} else if (nilai >= 75 && nilai < 85) {
  console.log('B');
} else if (nilai >= 65 && nilai < 75) {
  console.log('C');
} else if (nilai >= 55 && nilai < 65) {
  console.log('D');
} else if (nilai < 55 && nilai >= 0) {
  console.log('E');
} else {
  console.log('Pilih angka dari 0 sampai 100');
}

// soal 2
var tanggal = 16;
var bulan = 9;
var tahun = 1992;

// jawaban soal 2
switch (bulan) {
  case 1: {
    console.log(tanggal + ' ' + 'Januari' + ' ' + tahun);
    break;
  }
  case 2: {
    console.log(tanggal + ' ' + 'Februari' + ' ' + tahun);
    break;
  }
  case 3: {
    console.log(tanggal + ' ' + 'Maret' + ' ' + tahun);
    break;
  }
  case 4: {
    console.log(tanggal + ' ' + 'April' + ' ' + tahun);
    break;
  }
  case 5: {
    console.log(tanggal + ' ' + 'Mei' + ' ' + tahun);
    break;
  }
  case 6: {
    console.log(tanggal + ' ' + 'Juni' + ' ' + tahun);
    break;
  }
  case 7: {
    console.log(tanggal + ' ' + 'Juli' + ' ' + tahun);
    break;
  }
  case 8: {
    console.log(tanggal + ' ' + 'Agustus' + ' ' + tahun);
    break;
  }
  case 9: {
    console.log(tanggal + ' ' + 'September' + ' ' + tahun);
    break;
  }
  case 10: {
    console.log(tanggal + ' ' + 'Oktober' + ' ' + tahun);
    break;
  }
  case 11: {
    console.log(tanggal + ' ' + 'November' + ' ' + tahun);
    break;
  }
  case 12: {
    console.log(tanggal + ' ' + 'Desember' + ' ' + tahun);
    break;
  }
  default: {
    console.log('Pilih bulan dari 1 sampai 12');
  }
}

// soal 3
// menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi n dan alas n

// Jawaban 3
var n = 5;

for (var i = 1; i <= n; i++) {
  var segitiga = '';
  for (var j = 1; j <= i; j++) {
    segitiga += '#';
  }
  console.log(segitiga);
}

// soal 4
// suatu nilai m dengan tipe integer, dan buatlah pengulangan dari 1 sampai dengan m

// Output untuk m = 3

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===

// jawaban 4
var m = 17;

var digit = 1; // untuk penomoran
var garis = ''; // string kosong untuk pembatas loop
var output = ''; //string kosong untuk hasil output

while (digit <= m) {
  if (digit % 3 == 1) {
    output += digit + ' - I love Programming \n';
  } else if (digit % 3 == 2) {
    output += digit + ' - I love Javascript \n';
  } else {
    output += digit + ' - I love VueJS \n';
    for (let i = 1; i <= digit; i++) {
      garis += '=';
    }
    output += garis + '\n';
    garis = ''; // reset garis
  }
  digit++;
}
console.log(output);
